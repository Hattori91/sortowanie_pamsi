#include <iostream>
#include <string>
#include <ctime>



using namespace std;




class tablica{
	public:
		int *tab;								//Wskaznik na tablice danych 
		int *pom;								//Wskaznik na tablice pomocnicza ( dla algorytmu Mergesort)
		int wielkosc;
		int min;
		int max;
		bool sprawdz;
		tablica(int,int,int);
		tablica(int,int,int,bool);				//Kiedy argument typu bool jest true, konstruktor tworzy tablica pomocnicza
		void wyswietl(string);
};



class sortowanie{

	public:

	
		void merge(tablica);
		void quicksort(tablica,int,int);
		void kopcowanie(tablica,int);
		void przywroc(tablica,int,int);
		void scalanie(tablica,int,int);
		void scalanie_scalanietablicy(tablica,int,int);


};


tablica::tablica(int n , int min , int max)
{
	sprawdz = false;

	tab = new int[n];
	
	this->wielkosc=n;

	for(int i=0; i<wielkosc; i++)
		tab[i] = ( rand() % max )+ min;
}


tablica::tablica(int n , int min , int max, bool czy_pomocnicza)
{
	
	sprawdz = false;

	tab = new int[n];
	
	this->wielkosc=n;

	for(int i=0; i<wielkosc; i++)
		tab[i] = ( rand() % max )+ min;

	if( czy_pomocnicza == true)
	{
		pom = new int[n];
		sprawdz = true;
	}

}



void tablica::wyswietl(string tekst)
{
	cout << tekst << endl;

	for(int i=0;i<wielkosc;i++)
		cout << tab[i] << " ";

	cout << "\n" << endl;

}

void sortowanie::quicksort(tablica obiekt, int zakres_pocz, int zakres_kon)
{

      int i = zakres_pocz, j = zakres_kon;									
      
	  int tym;													//Zmienna pomocnicza do przechowania wartosci

      int srodek = obiekt.tab[(i + j) / 2];

      while (i <= j) 
	  {

            while (obiekt.tab[i] < srodek)
                  i++;
          
			while (obiekt.tab[j] > srodek)
                  j--;
          
			if (i <= j) 
			{
                  tym = obiekt.tab[i];
                  obiekt.tab[i] = obiekt.tab[j];
                  obiekt.tab[j] = tym;
                  i++;
                  j--;
            }

      }

      if (zakres_pocz < j)

            this->quicksort(obiekt, zakres_pocz, j);

      if (i < zakres_kon)

            this->quicksort(obiekt, i, zakres_kon);

}



void sortowanie::przywroc (tablica obiekt, int k, int ilosc )
{   // przywracanie kopca
	
	int i,j;
	
	i = obiekt.tab[k-1];
	
	while ( k <= ilosc/2 ) 
	{
		
		j=2*k;
		
		if ((j<ilosc) && ( obiekt.tab[j-1]<obiekt.tab[j] ) ) 
			j++;

		if ( i >= obiekt.tab[j-1] ) 

			break;
		else 
		{
			obiekt.tab[k-1] = obiekt.tab[j-1];
			k=j;
		}

	}

	obiekt.tab[k-1]=i;
}

void sortowanie::kopcowanie ( tablica obiekt, int ilosc ){ 
	int i, temp;
	
	for ( i = ilosc/2; i>0; i-- ) 
		
		this->przywroc(obiekt,i,ilosc);

	do {
			temp = obiekt.tab[0]; // zamiana miejscami zmiennych
			obiekt.tab[0]=obiekt.tab[ilosc-1];
			obiekt.tab[ilosc-1]=temp;
			ilosc--;
			this->przywroc(obiekt,1,ilosc);

		} while ( ilosc > 1 );
}

void sortowanie::scalanie_scalanietablicy(tablica obiekt, int poczatek, int koniec)
{
    for (int i = poczatek; i <= koniec; i++)
	{
	
		obiekt.pom[i] = obiekt.tab[i];

	}
 
	int p = poczatek;
	
	int q = (poczatek + koniec) / 2 + 1;
	
	int r = poczatek;
	
	while (p <= (poczatek + koniec) / 2 && q <= koniec)
	{
		if (obiekt.pom[p] < obiekt.pom[q])
		{
			obiekt.tab[r] = obiekt.pom[p];
			r++;
			p++;
		}
		
		else
		{
			obiekt.tab[r] = obiekt.pom[q];
			r++;
			q++;
		}
	}
 
	while (p <= (poczatek + koniec) / 2)
	{
		obiekt.tab[r] = obiekt.pom[p];
		r++;
		p++;
	}
}


void sortowanie::scalanie(tablica obiekt, int poczatek, int koniec)
{
    
	if( obiekt.sprawdz == false )
	{
		cout << "Obiekt nie posiada tablicy pomocniczej , ktora jest niezbedna do dzialania algorytmu" << endl;
		return;
	}
	


    if (poczatek < koniec)
    {
               
		int srodek = (koniec + poczatek) / 2;              
    
		this->scalanie( obiekt, poczatek, srodek );  
    
		this->scalanie( obiekt, srodek+1 , koniec);  
    
		this->scalanie_scalanietablicy( obiekt, poczatek, koniec );  
	}

}
 



int main(void)
{
	int n=1000000;

	int min=0;
	
	int i;

	double suma=0;

	int max=11;

	srand (time(NULL));

	clock_t start,koniec;
	
	tablica dane1(n,min,max);											//Konstruktor tworzy 10 losowych liczb o podanym zakresie
	tablica dane2(n,min,max);
	tablica dane3(n,min,max,true);
	
	sortowanie sortowanie;												//Tworzenie obiektu sortowanie

	



	//dane1.wyswietl("\nDane przed sortowaniem Quicksort:");

	for(i=0;i<3;i++)
	{
		start = clock();
		sortowanie.quicksort(dane1,0,n-1);
		koniec = clock();
		cout << "Czas realizacji algorytmu Quicksort dla tablicy " << n << " elementowej, wynosi: " << (double)(koniec-start) << " ms\n" << endl;
		suma += (double)(koniec-start);
	}

	cout << "Srednia czasu wykonywania Quicksort: " << suma/3 << "\n" << endl;

	suma=0;

	//dane1.wyswietl("\nDane po Quicksort: ");




	//dane2.wyswietl("\nDane przed sortowaniem Kopiec:");
	
	for(i=0;i<3;i++)
	{
		start = clock();
		sortowanie.kopcowanie(dane2,n);
		koniec = clock();
		cout << "Czas realizacji algorytmu Kopiec dla tablicy " << n << " elementowej, wynosi: " << (double)(koniec-start) << " ms\n" << endl;
		suma += (double)(koniec-start);

	}

	
	cout << "Srednia czasu wykonywania Kopiec: " << suma/3 << "\n" << endl;

	suma=0;

	//dane2.wyswietl("\nDane po Kopiec: ");
	

	//dane3.wyswietl("\nDane przed sortowaniem Scalanie:");

	for(i=0;i<3;i++)
	{
		start = clock();
		sortowanie.scalanie(dane3,0,n-1);
		koniec = clock();
		cout << "Czas realizacji algorytmu Scalanie dla tablicy " << n << " elementowej, wynosi: " << (double)(koniec-start) << " ms\n" << endl;
		suma += (double)(koniec-start);
	}

	cout << "Srednia czasu wykonywania Scalanie: " << suma/3 << "\n" << endl;

	suma=0;
	
	//dane3.wyswietl("\nDane po sortowaniu Scalanie:");
	


	system("Pause");
	return 0;
}

